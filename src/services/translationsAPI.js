import Vue from 'vue'

const ENDPOINT = 'https://www.mocky.io/v2/5ae1c5792d00004d009d7e5c'

export default {
  getTranslations () {
    return Vue.axios.get(ENDPOINT).then(res => res.data)
  },
  postTranslations (data = []) {
    return Vue.axios.post(ENDPOINT, data).then(res => res.data)
  }
}
