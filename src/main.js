import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import SvgIcon from 'vue-svgicon'
import layouts from './plugins/layouts'
import axios from 'axios'
import VueAxios from 'vue-axios'
import './assets/icons/dist/index.js'

Vue.use(VueAxios, axios)
Vue.use(SvgIcon, { tagName: 'svg-icon' })
Vue.use(layouts)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
