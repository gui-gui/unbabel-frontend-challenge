import Default from '@/layouts/Default.vue'

export default {
  install (Vue) {
    Vue.component('default-layout', Default)

    Vue.mixin({
      computed: {
        pageLayout () {
          return `${this.$route.meta.layout || 'default'}-layout`
        }
      }
    })
  }
}
