import API from '../../services/translationsAPI'

const state = {
  data: [],
  initialized: false,
  isFetching: false,
  isPosting: false
}

const actions = {
  fetch (context) {
    context.commit('toggleIsFetching')

    API.getTranslations()
      .then(data => context.commit('setData', data))
      .catch((error) => console.log(error))
      .then(() => {
        context.commit('initialized')
        context.commit('toggleIsFetching')
      })
  },

  post (context) {
    context.commit('toggleIsPosting')

    API.postTranslations(context.state.data)
      .then(data => context.commit('setData', data))
      .catch((error) => console.log(error))
      .then(() => context.commit('toggleIsPosting'))
  },

  new (context) {
    context.commit('initialized')
    context.commit('new')
  },

  delete (context, id) {
    context.commit('delete', id)
  },

  saveVoice (context, { id, voice }) {
    context.commit('saveVoice', { id, voice })
  },

  saveText (context, { id, text }) {
    context.commit('saveText', { id, text })
  }
}

const mutations = {
  setData (state, data) {
    state.data = data
  },

  new (state) {
    state.data.push({
      tempId: Date.now(),
      voice: 'Voice here',
      text: 'Your translation goes here'
    })
  },

  delete (state, id) {
    state.data = state.data.filter(t => {
      if (t.tempId) return t.tempId !== id
      return t.id !== id
    })
  },

  initialized (state) {
    state.initialized = true
  },

  toggleIsFetching (state) {
    state.isFetching = !state.isFetching
  },

  toggleIsPosting (state) {
    state.isPosting = !state.isPosting
  },

  saveVoice (state, { id, voice }) {
    const index = state.data.findIndex(t => t.id === id || t.tempId === id)
    state.data[index].voice = voice
  },

  saveText (state, { id, text }) {
    const index = state.data.findIndex(t => t.id === id || t.tempId === id)
    state.data[index].text = text
  }
}

const getters = {
  getTranslationById: (state) => (id) => {
    return state.data.find(t => t.id === id || t.tempId === id)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
