import Vue from 'vue'
import Vuex from 'vuex'
import translations from './modules/translations'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    translations
  },
  strict: debug
})
