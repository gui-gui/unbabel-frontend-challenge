import { createLocalVue, shallowMount } from '@vue/test-utils'
import VueRouter from 'vue-router'
import App from '@/App.vue'

const localVue = createLocalVue()
localVue.use(VueRouter)

const router = new VueRouter()
const wrapper = shallowMount(App, { localVue, router })

describe('App.vue', () => {
  it('is vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('has a mouse method that toggles isUsingMouse', () => {
    const isUsingMouse = wrapper.vm.isUsingMouse
    wrapper.vm.mouse(!isUsingMouse)
    expect(wrapper.vm.isUsingMouse).toBe(!isUsingMouse)
  })

  it('toggles is-using-mouse class when isUsingMouse changes', () => {
    wrapper.setData({ isUsingMouse: false })
    expect(wrapper.classes('is-using-mouse')).toBe(false)

    wrapper.setData({ isUsingMouse: true })
    expect(wrapper.classes('is-using-mouse')).toBe(true)
  })
})
