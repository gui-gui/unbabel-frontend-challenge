import translations from '@/store/modules/translations'
import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Store/modules/translations.js', () => {
  describe('getters', () => {
    it('should have "getTranslationById" getter', () => {
      let state = {
        data: [{
          id: 1,
          voice: 'Voice',
          text: 'Text'
        }]
      }

      const translation = translations.getters.getTranslationById(state)(1)
      expect(translation.id).toBe(1)
      expect(translation.voice).toBe('Voice')
      expect(translation.text).toBe('Text')
    })
  })

  describe('mutations', () => {
    let state

    beforeEach(() => {
      state = {
        data: [],
        initialized: false,
        isFetching: false,
        isPosting: false
      }
    })

    it('should have a "setData" mutation', () => {
      expect(state.data).toEqual([])
      translations.mutations.setData(state, ['1'])
      expect(state.data).toEqual(['1'])
    })

    it('should have a "new" mutation that inserts a new translation in the data array', () => {
      expect(state.data).toEqual([])
      translations.mutations.new(state)
      expect(typeof state.data[0].tempId).toBe('number')
      expect(typeof state.data[0].voice).toBe('string')
      expect(typeof state.data[0].text).toBe('string')
    })

    it('should have a "delete" mutation that deletes a translation given its id', () => {
      state.data = [{ id: 1 }, { id: 2 }]
      expect(state.data.length).toBe(2)
      translations.mutations.delete(state, 1)
      expect(state.data.length).toBe(1)
      expect(state.data[0].id).toBe(2)
    })

    it('should have a "initialized" mutation that sets initialized to true', () => {
      expect(state.initialized).toBe(false)
      translations.mutations.initialized(state)
      expect(state.initialized).toBe(true)
    })

    it('should have a "toggleIsFetching" mutation that toggles isFetching', () => {
      expect(state.isFetching).toBe(false)
      translations.mutations.toggleIsFetching(state)
      expect(state.isFetching).toBe(true)
      translations.mutations.toggleIsFetching(state)
      expect(state.isFetching).toBe(false)
    })

    it('should have a "toggleIsPosting" mutation that toggles isFetching', () => {
      expect(state.isPosting).toBe(false)
      translations.mutations.toggleIsPosting(state)
      expect(state.isPosting).toBe(true)
      translations.mutations.toggleIsPosting(state)
      expect(state.isPosting).toBe(false)
    })

    it('should have a "saveVoice" mutation that changes voice value given translation id', () => {
      state.data = [
        { id: 1, voice: 'Voice 1' },
        { id: 2, voice: 'Voice 2' }
      ]
      expect(state.data[0].voice).toBe('Voice 1')
      expect(state.data[1].voice).toBe('Voice 2')
      translations.mutations.saveVoice(state, { id: 1, voice: 'New Voice' })
      expect(state.data[0].voice).toBe('New Voice')
      expect(state.data[1].voice).toBe('Voice 2')
    })

    it('should have a "saveText" mutation that changes text value given translation id', () => {
      state.data = [
        { id: 1, text: 'Text 1' },
        { id: 2, text: 'Text 2' }
      ]
      expect(state.data[0].text).toBe('Text 1')
      expect(state.data[1].text).toBe('Text 2')
      translations.mutations.saveText(state, { id: 1, text: 'New Text' })
      expect(state.data[0].text).toBe('New Text')
      expect(state.data[1].text).toBe('Text 2')
    })
  })

  describe('actions', () => {
    let store
    let mockSetData
    let mockNew
    let mockDelete
    let mockInitialized
    let mockToggleIsFetching
    let mockToggleIsPosting
    let mockSaveVoice
    let mockSaveText

    beforeEach(() => {
      mockSetData = jest.fn()
      mockNew = jest.fn()
      mockDelete = jest.fn()
      mockInitialized = jest.fn()
      mockToggleIsFetching = jest.fn()
      mockToggleIsPosting = jest.fn()
      mockSaveVoice = jest.fn()
      mockSaveText = jest.fn()

      store = new Vuex.Store({
        state: {
          data: [],
          initialized: false,
          isFetching: false,
          isPosting: false
        },
        actions: translations.actions,
        mutations: {
          setData: mockSetData,
          new: mockNew,
          delete: mockDelete,
          initialized: mockInitialized,
          toggleIsFetching: mockToggleIsFetching,
          toggleIsPosting: mockToggleIsPosting,
          saveVoice: mockSaveVoice,
          saveText: mockSaveText
        }
      })
    })

    // TODO: Research a bit more into how to mock axios calls
    it('TODO: should have a "fetch" action that commits "toggleIsFetching" and then "setData", "initialized" and "toggleIsFetching" mutations after API call', () => {})
    it('TODO: should have a "post" action that commits "toggleIsPosting" and then "setData" and "toggleIsPosting" mutations after API call', () => {})

    it('should have a "new" action that commits "initialized" and "new" mutations', () => {
      expect(mockInitialized.mock.calls).toHaveLength(0)
      expect(mockNew.mock.calls).toHaveLength(0)
      return store.dispatch('new')
        .then(() => {
          expect(mockInitialized.mock.calls).toHaveLength(1)
          expect(mockNew.mock.calls).toHaveLength(1)
        })
    })

    it('should have a "delete" action that commits "delete" mutation', () => {
      expect(mockDelete.mock.calls).toHaveLength(0)
      return store.dispatch('delete')
        .then(() => {
          expect(mockDelete.mock.calls.length).toBe(1)
        })
    })

    it('should have a "saveVoice" action that commits "saveVoice" mutation', () => {
      expect(mockSaveVoice.mock.calls).toHaveLength(0)
      return store.dispatch('saveVoice', { id: 1, voice: 'Voice 1' })
        .then(() => {
          expect(mockSaveVoice.mock.calls.length).toBe(1)
        })
    })

    it('should have a "saveText" action that commits "saveText" mutation', () => {
      expect(mockSaveText.mock.calls).toHaveLength(0)
      return store.dispatch('saveText', { id: 1, text: 'Voice 1' })
        .then(() => {
          expect(mockSaveText.mock.calls.length).toBe(1)
        })
    })
  })
})
