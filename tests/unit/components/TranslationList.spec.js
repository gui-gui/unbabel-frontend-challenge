import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import TranslationList from '@/components/TranslationList.vue'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Components/TranslationList.vue', () => {
  let store
  let actions
  let stubs

  beforeEach(() => {
    actions = {
      new: jest.fn()
    }

    store = new Vuex.Store({
      modules: {
        translations: {
          namespaced: true,
          actions,
          state: {
            data: []
          }
        }
      }
    })

    stubs = ['svg-icon', 'translation']
  })

  it('is vue instance', () => {
    const wrapper = shallowMount(TranslationList, { store, stubs, localVue })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('dispatches "translations/new" when newTranslation is called', () => {
    const wrapper = shallowMount(TranslationList, { store, stubs, localVue })
    wrapper.vm.newTranslation()
    expect(actions.new).toHaveBeenCalled()
  })

  it('displays "No translations so far..." when empty', () => {
    const wrapper = shallowMount(TranslationList, { store, stubs, localVue })
    expect(wrapper.text()).toBe('No translations so far...')
  })

  it('displays translations when state.translations.data.length > 0', () => {
    const wrapper = shallowMount(TranslationList, { store, stubs, localVue })

    wrapper.vm.$store.state.translations.data = [{ id: 1 }]
    expect(wrapper.findAll('translation-stub').length).toBe(1)
    wrapper.vm.$store.state.translations.data.push({ id: 2 }, { id: 3 })
    expect(wrapper.findAll('translation-stub').length).toBe(3)
  })
})
