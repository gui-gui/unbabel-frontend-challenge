import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Translation from '@/components/Translation.vue'
import translations from '@/store/modules/translations'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Components/Translation.vue', () => {
  let store
  let actions
  let stubs
  let wrapper

  beforeEach(() => {
    actions = {
      delete: jest.fn(),
      saveVoice: jest.fn(),
      saveText: jest.fn()
    }

    store = new Vuex.Store({
      modules: {
        translations: {
          namespaced: true,
          actions,
          getters: translations.getters,
          state: {
            data: [{
              id: 1,
              voice: 'Voice 1',
              text: 'Text 1'
            }]
          }
        }
      }
    })

    stubs = ['svg-icon', 'translation']

    wrapper = shallowMount(Translation, {
      store,
      stubs,
      propsData: {
        translationId: 1
      },
      localVue
    })
  })

  it('is vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('dispatches "translations/delete" when deleteTranslation method is called', () => {
    wrapper.vm.deleteTranslation()
    expect(actions.delete).toHaveBeenCalled()
  })

  it('dispatches "translations/saveVoice" when saveVoice method is called', () => {
    wrapper.vm.saveVoice()
    expect(actions.saveVoice).toHaveBeenCalled()
  })

  it('dispatches "translations/saveText" when saveText method is called', () => {
    wrapper.vm.saveText()
    expect(actions.saveText).toHaveBeenCalled()
  })

  it('shows translation-voice input when data.editingVoice = true', () => {
    const input = wrapper.find({ ref: 'translation-voice' })
    expect(input.isVisible()).toBe(false)
    wrapper.setData({ editingVoice: true })
    expect(input.isVisible()).toBe(true)
  })

  it('shows translation-text input when data.editingText = true', () => {
    const input = wrapper.find({ ref: 'translation-text' })
    expect(input.isVisible()).toBe(false)
    wrapper.setData({ editingText: true })
    expect(input.isVisible()).toBe(true)
  })
})
