import Vuex from 'vuex'
import VueRouter from 'vue-router'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import router from '@/router'
import layouts from '@/plugins/layouts'
import Home from '@/views/Home.vue'

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(VueRouter)
localVue.use(layouts)

describe('views/Home.vue', () => {
  let store
  let actions
  let stubs

  beforeEach(() => {
    actions = {
      fetch: jest.fn(),
      post: jest.fn()
    }

    store = new Vuex.Store({
      modules: {
        translations: {
          namespaced: true,
          actions,
          state: {}
        }
      }
    })

    stubs = ['svg-icon']
  })

  it('is vue instance', () => {
    const wrapper = shallowMount(Home, { store, router, stubs, localVue })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should use default-layout', () => {
    const wrapper = shallowMount(Home, { store, router, stubs, localVue })
    expect(wrapper.vm.pageLayout).toBe('default-layout')
  })

  it('dispatches "translations/fetch" when fetchTranslations is called', () => {
    const wrapper = shallowMount(Home, { store, router, stubs, localVue })
    wrapper.vm.fetchTranslations()
    expect(actions.fetch).toHaveBeenCalled()
  })

  it('dispatches "translations/post" when postTranslations is called', () => {
    const wrapper = shallowMount(Home, { store, router, stubs, localVue })
    wrapper.vm.postTranslations()
    expect(actions.post).toHaveBeenCalled()
  })
})
