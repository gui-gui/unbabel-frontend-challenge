import { mount } from '@vue/test-utils'
import Default from '@/layouts/Default.vue'

const wrapper = mount(Default, {
  slots: {
    header: 'header content',
    default: 'main content'
  }
})

describe('Layouts/Default.vue', () => {
  it('is vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  // TODO: wait this fix to be ready and uncomment these assertions
  // https://github.com/vuejs/vue-test-utils/issues/967
  // Will propably be released with v1.0.0-beta.26

  // it('renders header slot content properly', () => {
  //   expect(wrapper.find('header').text()).toBe('header content')
  // })

  // it('renders default slot content properly', () => {
  //   expect(wrapper.find('main').text()).toBe('main content')
  // })
})
